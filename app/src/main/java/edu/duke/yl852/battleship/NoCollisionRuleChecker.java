package edu.duke.yl852.battleship;

public class NoCollisionRuleChecker<T> extends PlacementRuleChecker<T> {
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        for (Coordinate c : theShip.getCoordinates()) {
            if (theBoard.whatIsAtForSelf(c)!=null) {
                return "Already a ship at this location";
            }
        }
        return null;
    }

    public NoCollisionRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }
}
