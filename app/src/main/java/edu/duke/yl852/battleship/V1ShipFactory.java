package edu.duke.yl852.battleship;

import java.util.HashSet;

public class V1ShipFactory implements AbstractShipFactory<Character> {
    
    // create the ship and distinguish the orientation of the ship
    protected Ship<Character> createShip(Placement where, int w, int h, char letter, String name) {
        Ship<Character> ship = null;
        if (where.getOrientation() == 'H') {
            ship = new RectangleShip(name, where.getCoordiante(), h, w, letter, '*', 'H');
        } else {
            ship = new RectangleShip(name, where.getCoordiante(), w, h, letter, '*', 'V');
        }
        return ship;
    }

    //Make coordinate hashset for part of NON-Rectangle shaped ship
    protected HashSet<Coordinate> makeCoords(Coordinate upperLeft, int width, int height) {
        HashSet<Coordinate> out = new HashSet<>();
        int curRow = upperLeft.getRow();
        int curColumn = upperLeft.getColumn();
        for (int widthCount = 0; widthCount < width; widthCount++) {
            int tempRow = curRow;
            for (int heightCount = 0; heightCount < height; heightCount++) {
                out.add(new Coordinate(tempRow, curColumn));
                tempRow++;
            }
            curColumn++;
        }
        return out;
    }

    @Override
    public Ship<Character> makeSubmarine(Placement where) {
        return createShip(where, 1, 2, 's', "Submarine");
    }

    @Override
    public Ship<Character> makeBattleship(Placement where) {
        Ship<Character> ship = null;
        char orientation = where.getOrientation();
        HashSet<Coordinate> coordinates = new HashSet<>();
        Coordinate coord = null;
        switch(orientation) {
            case 'U':
                coord = new Coordinate(where.getCoordiante().getRow()+1, where.getCoordiante().getColumn());
                coordinates = makeCoords(coord,3,1);
                coordinates.add(new Coordinate(coord.getRow()-1, coord.getColumn()+1));
                ship = new RectangleShip(coordinates,
                        new SimpleShipDisplayInfo('b', '*'),
                        new SimpleShipDisplayInfo(null, 'b'),
                        "BattleShip", '0', where);
                break;

            case 'R':
                coord = new Coordinate(where.getCoordiante().getRow(), where.getCoordiante().getColumn());
                coordinates = makeCoords(coord,1,3);
                coordinates.add(new Coordinate(coord.getRow()+1, coord.getColumn()+1));
                ship = new RectangleShip(coordinates,
                        new SimpleShipDisplayInfo('b', '*'),
                        new SimpleShipDisplayInfo(null, 'b'),
                        "BattleShip", '1', where);
                break;

            case 'D':
                coord = new Coordinate(where.getCoordiante().getRow(), where.getCoordiante().getColumn());
                coordinates = makeCoords(coord,3,1);
                coordinates.add(new Coordinate(coord.getRow()+1, coord.getColumn()+1));
                ship = new RectangleShip(coordinates,
                        new SimpleShipDisplayInfo('b', '*'),
                        new SimpleShipDisplayInfo(null, 'b'),
                        "BattleShip",'2', where);
                break;

            case 'L':
                coord = new Coordinate(where.getCoordiante().getRow(), where.getCoordiante().getColumn()+1);
                coordinates = makeCoords(coord,1,3);
                coordinates.add(new Coordinate(coord.getRow()+1, coord.getColumn()-1));
                ship = new RectangleShip(coordinates,
                        new SimpleShipDisplayInfo('b', '*'),
                        new SimpleShipDisplayInfo(null, 'b'),
                        "BattleShip",'3', where);
                break;
        }

        return ship;
    }

    @Override
    public Ship<Character> makeCarrier(Placement where) {
        Ship<Character> ship = null;
        char orientation = where.getOrientation();
        HashSet<Coordinate> coordinates = new HashSet<>();
        Coordinate coord = null;
        switch(orientation) {
            case 'U':
                coord = new Coordinate(where.getCoordiante().getRow(), where.getCoordiante().getColumn());
                coordinates = makeCoords(coord,1,4);
                coordinates.addAll(makeCoords(new Coordinate(where.getCoordiante().getRow()+2, where.getCoordiante().getColumn()+1),
                        1,3));
                ship = new RectangleShip(coordinates,
                        new SimpleShipDisplayInfo('c', '*'),
                        new SimpleShipDisplayInfo(null, 'c'),
                        "Carrier",'0', where);
                break;

            case 'R':
                coord = new Coordinate(where.getCoordiante().getRow(), where.getCoordiante().getColumn()+1);
                coordinates = makeCoords(coord,4,1);
                coordinates.addAll(makeCoords(new Coordinate(where.getCoordiante().getRow()+1, where.getCoordiante().getColumn()),
                        3,1));
                ship = new RectangleShip(coordinates,
                        new SimpleShipDisplayInfo('c', '*'),
                        new SimpleShipDisplayInfo(null, 'c'),
                        "Carrier",'1', where);
                break;

            case 'D':
                coord = new Coordinate(where.getCoordiante().getRow()+1, where.getCoordiante().getColumn()+1);
                coordinates = makeCoords(coord,1,4);
                coordinates.addAll(makeCoords(new Coordinate(where.getCoordiante().getRow(), where.getCoordiante().getColumn()),
                        1,3));
                ship = new RectangleShip(coordinates,
                        new SimpleShipDisplayInfo('c', '*'),
                        new SimpleShipDisplayInfo(null, 'c'),
                        "Carrier",'2', where);
                break;

            case 'L':
                coord = new Coordinate(where.getCoordiante().getRow()+1, where.getCoordiante().getColumn());
                coordinates = makeCoords(coord,4,1);
                coordinates.addAll(makeCoords(new Coordinate(where.getCoordiante().getRow(), where.getCoordiante().getColumn()+2),
                        3,1));
                ship = new RectangleShip(coordinates,
                        new SimpleShipDisplayInfo('c', '*'),
                        new SimpleShipDisplayInfo(null, 'c'),
                        "Carrier",'3', where);
                break;
        }

        return ship;
    }

    @Override
    public Ship<Character> makeDestroyer(Placement where) {
        return createShip(where, 1, 3, 'd', "Destroyer");
    }
}
