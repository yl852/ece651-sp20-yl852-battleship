package edu.duke.yl852.battleship;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Reader;
import java.io.StringReader;  

import java.io.*;

public class App {
  
  TextPlayer p1;
  TextPlayer p2;
  
  public App(TextPlayer a, TextPlayer b) {
    p1 = a;
    p2 = b;
  }

//  public void doPlacementPhase() throws IOException {
//    p1.doPlacementPhase();
//    p2.doPlacementPhase();
//  }

  public void doPlacementPhaseFor(TextPlayer player) throws IOException {
    player.doPlacementPhase();
  }

  public String result_check() {
    if(p1.theBoard.allSunk()) {
      return "Player "+p2.name+" is the winner";
    } else if (p2.theBoard.allSunk()) {
      return "Player "+p1.name+" is the winner";
    }
    return null;
  }




  public static void main(String args[]) throws IOException {
    Reader isr = new InputStreamReader(System.in);
    PrintStream out = System.out; 
    Board<Character> board = new BattleShipBoard<>(10,20, 'X');
    Board<Character> board1 = new BattleShipBoard<>(10,20, 'X');
    BufferedReader buffer = new BufferedReader(isr);
    out.println("Do you want player 1 to be computer? (y/n)");
    String s = buffer.readLine();
    TextPlayer player1 = new TextPlayer("A", board, buffer, out, new V1ShipFactory());
    TextPlayer player2 = new TextPlayer("B", board1, buffer, out, new V1ShipFactory());
    AbstractShipFactory f = new V1ShipFactory();
    if (s.equals("y")) {
      Placement v1_1 = new Placement(new Coordinate(1, 1), 'V');
      Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
      Ship<Character> dst = f.makeDestroyer(v1_1);
      Ship<Character> sub = f.makeSubmarine(v1_2);
      player1.theBoard.tryAddShip(dst);
      player1.theBoard.tryAddShip(sub);
    }
    out.println("Do you want player 2 to be computer? (y/n)");
    String s1 = buffer.readLine();
    if (s1.equals("y")) {
      Placement v1_1 = new Placement(new Coordinate(1, 1), 'V');
      Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
      Ship<Character> dst = f.makeDestroyer(v1_1);
      Ship<Character> sub = f.makeSubmarine(v1_2);
      player2.theBoard.tryAddShip(dst);
      player2.theBoard.tryAddShip(sub);
    }

    App app = new App(player1, player2);
    if (s.equals("n")) {
      app.doPlacementPhaseFor(player1);
    }
    if (s1.equals("n")) {
      app.doPlacementPhaseFor(player2);
    }
//    app.doPlacementPhase();
    int rowCount = 1;
    int colCount = 1;
    while (app.result_check() == null) {
      if (colCount == 9) {
        rowCount++;
        colCount = 1;
      }
      if (s.equals("n")) {
        app.p1.playOneRound(app.p2.theBoard);
      } else {
        app.p2.theBoard.fireAt(new Coordinate(rowCount, colCount));
      }
      if (s1.equals("n")) {
        app.p2.playOneRound(app.p1.theBoard);
      } else {
        app.p1.theBoard.fireAt(new Coordinate(rowCount, colCount));
      }
      colCount++;
    }
    out.println(app.result_check());
  }

}
