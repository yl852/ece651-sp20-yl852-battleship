package edu.duke.yl852.battleship;

public class Placement {
    final Coordinate where;
    final char orientation;

    //Constructor using Coordinate and char input
    public Placement(Coordinate where, char orientation) {
        this.where = where;
        this.orientation = Character.toUpperCase(orientation);
    }

    public Coordinate getCoordiante() {
        return where;
    }

    public char getOrientation() {
        return orientation;
    }

    //Constructor using string input
    public Placement(String descr) {
        String input = descr.toUpperCase();
        //Check the cases need to be handled and throw exception
        if (input.length() != 3 || !Character.isLetter(input.charAt(0)) || !Character.isDigit(input.charAt(1))) {
            throw new IllegalArgumentException(
                "Placement exceeds board limit");
        } else if (input.charAt(2) != 'V' && input.charAt(2) != 'H' && input.charAt(2) != 'U' && input.charAt(2) != 'D' && input.charAt(2) != 'R' && input.charAt(2) != 'L') {
            throw new IllegalArgumentException(
                "Invalid Ship orientation");
        }
        //Getting the first two letters using substring method to create a new Coordinate object and stored in this.where
        this.where = new Coordinate(input.substring(0,2));
        this.orientation = input.charAt(2);
    }

    @Override
    public String toString() {
        return "(" + where.getRow() + ", " + where.getColumn() + ", " + orientation + ")";
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (o.getClass().equals(getClass())) {
            Placement c = (Placement) o;
            return where.equals(c.getCoordiante()) && orientation == c.getOrientation();
        }
        return false;
    }
}
