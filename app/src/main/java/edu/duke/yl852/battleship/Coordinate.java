package edu.duke.yl852.battleship;

public class Coordinate {
    private final int row;
    private final int column;

    public int getRow() {
        return row;
    }

    public int getColumn() {
        return column;
    }

    //Constructor using direct int input
    public Coordinate(int row, int column) {
        this.row = row;
        this.column = column;
    }

    //Constructor using String input
    public Coordinate(String descr) {
        String input = descr.toUpperCase();
        //Handle the cases need to throw exception
        if (input.length() != 2 || (!Character.isLetter(input.charAt(0))) || (!Character.isDigit(input.charAt(1)))) {
            throw new IllegalArgumentException(
                "Input invalid");
        }
        //get the number difference between the input and the corresponding first letter and first number to get the row and column value
        this.row = input.charAt(0)-'A';
        this.column = input.charAt(1)-'0';
    }

    @Override
    public boolean equals(Object o) {
        if (o.getClass().equals(getClass())) {
            Coordinate c = (Coordinate) o;
            //compare both row and column to determine if the object is equal to LHS
            return row == c.row && column == c.column;
        }
        return false;
    }

    @Override
    public String toString() {
        return "(" + row + ", " + column + ")";
    }

    @Override
    public int hashCode() {
        return toString().hashCode();
    }

}
