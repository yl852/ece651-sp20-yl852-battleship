package edu.duke.yl852.battleship;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.Reader;

import java.util.*;
import java.util.HashMap;
import java.util.function.Function;

public class TextPlayer {
    final AbstractShipFactory<Character> shipFactory;
    final Board<Character> theBoard;
    final BoardTextView view;
    final BufferedReader inputReader;
    final PrintStream out; 
    String name;
    final ArrayList<String> shipsToPlace;
    final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;
    int moveCount;
    int scanCount;


    public TextPlayer(String name, Board<Character> theBoard, BufferedReader inputSource, PrintStream out, AbstractShipFactory shipFactory) {
        this.theBoard = theBoard;
        this.view = new BoardTextView(theBoard);
        this.inputReader = inputSource;
        this.out = out;
        this.shipFactory = shipFactory;
        this.name = name;
        shipsToPlace = new ArrayList<>();
        shipCreationFns = new HashMap();
        moveCount = 3;
        scanCount = 3;
        setupShipCreationMap();
        setupShipCreationList();
    }

    //print out the where the ships are being placed
    public Placement readPlacement(String prompt) throws IOException {
        out.println(prompt);
        String s = inputReader.readLine();
        System.out.println(s);
        return new Placement(s);
    }

    //add one ship to a desired place
    public void doOnePlacement() throws IOException {
        String prompt = "Player " + name + " where do you want to place a Destroyer?";
        Placement place = readPlacement(prompt);
        Ship<Character> ship  = shipFactory.makeDestroyer(place);
        theBoard.tryAddShip(ship);
        out.println(view.displayMyOwnBoard()); 
    }

    public void doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) throws IOException {
        Placement p = readPlacement("Player " + name + " where do you want to place a " + shipName + "?");
        Ship<Character> s = createFn.apply(p);
        theBoard.tryAddShip(s);
        out.print(view.displayMyOwnBoard());
    }
    
    protected void setupShipCreationMap() {
        shipCreationFns.put("Submarine", (p) -> shipFactory.makeSubmarine(p));
        shipCreationFns.put("Destroyer", (p)->shipFactory.makeDestroyer(p));
        shipCreationFns.put("Carrier", (p) -> shipFactory.makeCarrier(p));
        shipCreationFns.put("BattleShip", (p) -> shipFactory.makeBattleship(p));
    }

    protected void setupShipCreationList() {
        shipsToPlace.addAll(Collections.nCopies(1, "Submarine"));
        shipsToPlace.addAll(Collections.nCopies(1, "Destroyer"));
    }

    public void doPlacementPhase() throws IOException {
        out.println(view.displayMyOwnBoard());
        out.println("Player " + name + ": you are going to place the following ships (which are all\nrectangular). For each ship, type the coordinate of the upper left\n"+
        "side of the ship, followed by either H (for horizontal) or V (for\nvertical).  For example M4H would place a ship horizontally starting\nat M4 and going to the right.");
        for (String s : shipsToPlace) {
            doOnePlacement(s, shipCreationFns.get(s));
        }
    }

//    public void playOneTurn(Board enemyBoard, BoardTextView enemyTextView, BufferedReader inputSource) throws IOException {
//        String s = inputReader.readLine();
//        Coordinate c = new Coordinate(s.charAt(0)-'0', s.charAt(2)-'0');
//        enemyTextView.displayEnemyBoard();//shows the enemy board
//        Ship ship = enemyBoard.fireAt(c);//attack a certain coordinate
//        if (ship == null) {
//            out.println("You missed");
//        }
//    }
    public void playOneRound(Board enemyBoard) throws IOException {
            out.println("---------------------------------------------------------------------------\n" +
                    "Possible actions for Player "+name+":\n\n"+
                    " F Fire at a square\n" +
                    " M Move a ship to another square ("+moveCount+" remaining)\n" +
                    " S Sonar scan ("+scanCount+" remaining)\n\n"+
                    "Player "+name+", what would you like to do?\n" +
                    "---------------------------------------------------------------------------");
            String s = inputReader.readLine();
            if (s.equals("F")) {
                doAttackingPhase(enemyBoard);
            } else if (s.equals("M")) {
                doMovePhase();
            } else {
                doSonarScan();
            }
    }
    public void playOneTurn(Board enemyBoard) throws IOException {
        String prompt = "Player " + name + ": please enter a coordinate where you want to attack.";
        Coordinate coordinate = readCoordinate(prompt);
        Ship ship = enemyBoard.fireAt(coordinate);
        if (ship == null) {
            out.println("You missed");
        } else {
            BoardTextView btv = new BoardTextView(enemyBoard);
            out.println(btv.displayEnemyBoard());
        }
    }

    public void doAttackingPhase(Board enemyBoard) throws IOException {
        BoardTextView btv = new BoardTextView(enemyBoard);
        out.println(btv.displayEnemyBoard());
        playOneTurn(enemyBoard);
    }

    public Coordinate readCoordinate(String prompt) throws IOException {
        out.println(prompt);
        String s = inputReader.readLine();
        System.out.println(s);
        return new Coordinate(s.charAt(0)-'A', s.charAt(1)-'0');
    }
    public void moveShip() throws IOException {
        String prompt = "Player " + name + ": please enter a coordinate which is a part of the ship you want to move.";
        Coordinate coordinate = readCoordinate(prompt);
        Ship temp = theBoard.removeShip(coordinate);
        prompt = "Player " + name + ": please enter a new placement which the selected ship move to.";
        Placement placement = readPlacement(prompt);
        String name = temp.getName();
        Ship newShip = shipCreationFns.get(name).apply(placement);
        if (name.equals("Carrier") || name.equals("BattleShip")) {
            temp.setTopLeft(findUpTopLeft(temp));
            for (int i = 0; i < temp.getOrientation()-'0'; i++) {
                temp.ccwRotateShip();
            }

            for (int i = 0; i < newShip.getOrientation()-'0'; i++) {
                temp.cwRotateShip();
            }
            Iterator<Coordinate> itr = temp.getCoordinates().iterator();
            Coordinate newHit = null;
            Coordinate oldTopLeft = temp.getTopLeft();
            Coordinate newTopLeft = findUpTopLeft(newShip);
            while (itr.hasNext()) {
                Coordinate c = itr.next();
                if (temp.wasHitAt(c)) {
                    newHit = new Coordinate(newTopLeft.getRow() + (c.getRow() - oldTopLeft.getRow()),
                            newTopLeft.getColumn() + (c.getColumn() - oldTopLeft.getColumn()));
                    newShip.recordHitAt(newHit);
                }
            }
        } else {
            if (temp.getOrientation() == 'H') {
                temp.cwRotateShip();
            } else {
                temp.ccwRotateShip();
            }
            Iterator<Coordinate> itr = temp.getCoordinates().iterator();
            Coordinate newHit = null;
            Coordinate oldTopLeft = temp.getTopLeft();
            Coordinate newTopLeft = newShip.getTopLeft();
            while (itr.hasNext()) {
                Coordinate c = itr.next();
                if (temp.wasHitAt(c)) {
                    newHit = new Coordinate(newTopLeft.getRow() + (c.getRow() - oldTopLeft.getRow()),
                            newTopLeft.getColumn() + (c.getColumn() - oldTopLeft.getColumn()));
                    newShip.recordHitAt(newHit);
                }
            }
        }
        theBoard.tryAddShip(newShip);
    }

    public Coordinate findUpTopLeft(Ship temp) {
        Coordinate out = null;
        char orientation = temp.getOrientation();
        if (temp.getName().equals("Carrier")) {
            if (orientation == '1') {
                out = new Coordinate(temp.getTopLeft().getRow(), temp.getTopLeft().getColumn()+4);
            } else if (orientation == '2') {
                out = new Coordinate(temp.getTopLeft().getRow()+4, temp.getTopLeft().getColumn()+1);
            } else if (orientation == '3') {
                out = new Coordinate(temp.getTopLeft().getRow()+1, temp.getTopLeft().getColumn());
            } else {
                out = temp.getTopLeft();
            }
        } else if (temp.getName().equals("BattleShip")) {
            if (orientation == '1') {
                out = new Coordinate(temp.getTopLeft().getRow(), temp.getTopLeft().getColumn()+1);
            } else if (orientation == '2') {
                out = new Coordinate(temp.getTopLeft().getRow()+1, temp.getTopLeft().getColumn()+2);
            } else if (orientation == '3') {
                out = new Coordinate(temp.getTopLeft().getRow()+2, temp.getTopLeft().getColumn());
            } else {
                out = temp.getTopLeft();
            }
        }
        return out;
    }

    public void doMovePhase() throws IOException {
        if (moveCount == 0) {
            return;
        }
        out.println(view.displayMyOwnBoard());
        moveShip();
        moveCount--;
    }

    public void sonarScan() throws IOException {
        String prompt = "Player " + name + ": please enter a coordinate where you want to do sonar scan.";
        Coordinate coordinate = readCoordinate(prompt);
        int row = coordinate.getRow();
        int col = coordinate.getColumn();
        HashMap<String, Integer> hashMap = new HashMap<>();
        hashMap.put("Submarine", 0);
        hashMap.put("BattleShip", 0);
        hashMap.put("Carrier", 0);
        hashMap.put("Destroyer", 0);
        ArrayList<Ship<Character>> list = theBoard.getMyShips();
        HashSet<Coordinate> hashSet = new HashSet<>();
        hashSet.add(coordinate);

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4-i; j++) {
                hashSet.add(new Coordinate(row+i, col+j));
                hashSet.add(new Coordinate(row+i, col-j));
                hashSet.add(new Coordinate(row-i, col+j));
                hashSet.add(new Coordinate(row-i, col-j));
            }
        }

        for (Ship s : list) {
            for (Object key:s.getCoordinates()) {
                if (hashSet.contains(key)) {
                    hashMap.put(s.getName(), hashMap.get(s.getName())+1);
//                    for (Object k:s.getCoordinates()) {
//                        hashSet.remove(k);
//                    }
                }
            }
        }

        out.println("---------------------------------------------------------------------------\n" +
                "Submarines occupy "+hashMap.get("Submarine")+" squares.\n" +
                "Destroyers occupy "+hashMap.get("Destroyer")+" squares.\n" +
                "Battleships occupy "+hashMap.get("BattleShip")+" squares.\n" +
                "Carriers occupy "+hashMap.get("Carrier")+" squares.\n" +
                "---------------------------------------------------------------------------");
    }

    public void doSonarScan() throws IOException {
        if (scanCount == 0) {
            return;
        }
        sonarScan();
        scanCount--;
    }

}
