package edu.duke.yl852.battleship;

import java.util.ArrayList;
import java.util.function.Function;

/**
 * This class handles textual display of
 * a Board (i.e., converting it to a string to show
 * to the user).
 * It supports two ways to display the Board:
 * one for the player's own board, and one for the 
 * enemy's board.
 */


public class BoardTextView {
    // the board want to view
    private final Board<Character> toDisplay;
    /**
     * Constructs a BoardView, given the board it will display.
     * 
     * @param toDisplay is the Board to display
     * @throws IllegalArgumentException if the board is larger than 10x26.  
     */

    public BoardTextView(Board<Character> toDisplay) {
        this.toDisplay = toDisplay;
        if (toDisplay.getWidth() > 10 || toDisplay.getHeight() > 26) {
            throw new IllegalArgumentException(
                "The size of the board need to be smaller than 10x26, but it is: " + toDisplay.getWidth() + "x" + toDisplay.getHeight());
        }
    }

    protected String displayAnyBoard(Function<Coordinate, Character> getSquareFn) {
        StringBuilder ans = new StringBuilder(); //initialize the output answer
        String header = makeHeader(); //initialize the header
        ans.append(header);
        char letter = 'A'; //initialize the letter for the first row
        //loop through all the rows and assign the corresponding content for each row
        for (int row = 0; row < toDisplay.getHeight(); row++) {
            ans.append(letter); 
            ans.append(" ");
            for (int col = 0; col < toDisplay.getWidth()-1; col++) {
                Coordinate loc = new Coordinate(row,col);
                if (getSquareFn.apply(loc) != null) {
                    ans.append(getSquareFn.apply(loc));
                    ans.append("|");
                } else {
                    ans.append(" |");
                }
            }
            ans.append("  ");
            ans.append(letter);
            ans.append("\n");
            letter++;
        }
        //add the last row of the file
        ans.append(header);
        return ans.toString();
    }

    public String displayMyOwnBoard() {
        return displayAnyBoard((c)->toDisplay.whatIsAtForSelf(c));
    }

    public String displayEnemyBoard() {
        return displayAnyBoard((c)->toDisplay.whatIsAtForEnemy(c));
    }


    /**
     * This makes the header line, e.g. 0|1|2|3|4\n
     * 
     * @return the String that is the header line for the given board
     */

    String makeHeader() {
        StringBuilder ans = new StringBuilder("  "); // README shows two spaces at
        String sep=""; //start with nothing to separate, then switch to | to separate
        for (int i = 0; i < toDisplay.getWidth(); i++) {
            ans.append(sep);
            ans.append(i);
            sep = "|";
        }
        ans.append("\n");
        
        return ans.toString();
    }

    public String displayMyBoardWithEnemyNextToIt(BoardTextView enemyView, String myHeader, String enemyHeader) {
        String myboard = displayMyOwnBoard();
        String enemyboard = enemyView.displayMyOwnBoard();
        String myArray[] = myboard.split("\\r?\\n");
        String enemyArray[] = enemyboard.split("\\r?\\n");
        String out = new String();
        StringBuilder header = new StringBuilder();
        //header.append("  ");
        header.append(myArray[0]);
        header.append("               ");
        header.append(enemyArray[0]);
        StringBuilder temp = new StringBuilder();
        out+=header.toString();
        out+="\n";
        for (int i = 1; i < myArray.length-1; i++) {
            temp.append(myArray[i]);
            temp.append("             ");
            temp.append(enemyArray[i]);
            temp.append("\n");
            out+=temp.toString();
            temp.setLength(0);
        }
        out+=header.toString();
        out+="\n";
        return out;
    }

}
