package edu.duke.yl852.battleship;

import java.util.ArrayList;

public interface Board<T> {
  public int getWidth();

  public int getHeight();

  //Takes a ship (Basicship for now) object as a input and add to the myShips arrylist
  public String tryAddShip(Ship<T> toAdd);

  //Takes a coordinate object as input to display if there is a ship at the target coordinate
  public T whatIsAtForSelf(Coordinate where);

  //Takes a coordinate object as an input to check if there is a ship on the board being hit in this location, if not record the location in
  // the enemy miss hashset
  public Ship<T> fireAt(Coordinate c);

  public T whatIsAtForEnemy(Coordinate where);

  public boolean allSunk();

  //remove certain ship and return the ship
  public Ship removeShip(Coordinate coordinate);

  public ArrayList<Ship<T>> getMyShips();

}
