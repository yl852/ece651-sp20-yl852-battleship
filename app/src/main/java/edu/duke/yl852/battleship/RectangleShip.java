package edu.duke.yl852.battleship;

import java.util.HashSet;

public class RectangleShip<T> extends Basicship<T>{

    final String name;
    Coordinate topLeft;
    final char orientation;

    static HashSet<Coordinate> makeCoords(Coordinate upperLeft, int width, int height) {
        HashSet<Coordinate> out = new HashSet<>();
        int curRow = upperLeft.getRow();
        int curColumn = upperLeft.getColumn();
        for (int widthCount = 0; widthCount < width; widthCount++) {
            int tempRow = curRow;
            for (int heightCount = 0; heightCount < height; heightCount++) {
                out.add(new Coordinate(tempRow, curColumn));
                tempRow++;
            }
            curColumn++;
        }
        return out;
    }

    public char getOrientation() {
        return orientation;
    }

    public void setTopLeft(Coordinate coordinate) {
        this.topLeft = coordinate;
    }

    public Coordinate getTopLeft() {
        return topLeft;
    }
//    public RectangleShip(Iterable<Coordinate> where, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo, String name, Coordinate topLeft) {
//        super(where, myDisplayInfo, enemyDisplayInfo);
//        this.name = name;
//        this.topLeft = topLeft;
//    }

//    public RectangleShip(Iterable<Coordinate> where, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo, String name, char orientation) {
//        super(where, myDisplayInfo, enemyDisplayInfo);
//        this.name = name;
//        this.topLeft = null;
//        this.orientation = orientation;
//    }

    public RectangleShip(Iterable<Coordinate> where, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo, String name, char orientation, Placement place) {
        super(where, myDisplayInfo, enemyDisplayInfo);
        this.name = name;
        this.topLeft = place.getCoordiante();
        this.orientation = orientation;
    }
    //rectangle ship constructor
    public RectangleShip(String name, Coordinate upperLeft, int width, int height, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> shipDisplayInfo, char orientation) {
        super(makeCoords(upperLeft, width, height), myDisplayInfo, shipDisplayInfo);
        this.name = name;
        this.topLeft = upperLeft;
        this.orientation = orientation;
    }
    public RectangleShip(String name, Coordinate upperLeft, int width, int height, T data, T onHit, char orientation) {
        this(name, upperLeft, width, height, new SimpleShipDisplayInfo<T>(data, onHit),
                new SimpleShipDisplayInfo<T>(null, data), orientation);
    }
    public RectangleShip(Coordinate upperLeft, T data, T onHit) {
        this("testship", upperLeft, 1, 1, data, onHit, 'V');
    }
    

    public String getName() {
        return name;
    }
    
}
