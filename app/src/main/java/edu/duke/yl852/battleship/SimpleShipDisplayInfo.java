package edu.duke.yl852.battleship;

public class SimpleShipDisplayInfo<T> implements ShipDisplayInfo<T> {

    T myData;
    T onHit;

    public T getInfo(Coordinate where, boolean hit) {
        if (hit) {
            return onHit;
        }
        return myData;
    }

    public SimpleShipDisplayInfo(T myData, T onHit) {
        this.myData = myData;
        this.onHit = onHit;
    }

}
