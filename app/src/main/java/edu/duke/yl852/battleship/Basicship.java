package edu.duke.yl852.battleship;

import java.util.HashMap;
import java.util.Iterator;

public abstract class Basicship<T> implements Ship<T> {

  protected ShipDisplayInfo<T> myDisplayInfo;
  protected HashMap<Coordinate, Boolean> myPieces;
  protected ShipDisplayInfo<T> enemyDisplayInfo;
  
  //basic ship constructor
  public Basicship(Iterable<Coordinate> where, ShipDisplayInfo<T> myDisplayInfo, ShipDisplayInfo<T> enemyDisplayInfo) {
    myPieces = new HashMap<Coordinate, Boolean>();
    for (Coordinate coord : where) {
      myPieces.put(coord, false);
    }
    this.myDisplayInfo = myDisplayInfo;
    this.enemyDisplayInfo = enemyDisplayInfo;
  }


  @Override
  public boolean occupiesCoordinates(Coordinate where) {
    // TODO Auto-generated method stub
    if (myPieces.containsKey(where)) {
      return true;
    }
    return false;
  }
  //check if the ship is sunk
  @Override
  public boolean isSunk() {
    // TODO Auto-generated method stub
    for (Coordinate key : myPieces.keySet()) {
      if (myPieces.get(key) == false) {
        return false;
      }
    }
    return true;
  }
  //record the place where the ship was hitted
  @Override
  public void recordHitAt(Coordinate where) {
    // TODO Auto-generated method stub
    checkCoordinateInThisShip(where);
    myPieces.put(where, true);
  }
  //check if the ship is hit at certain place
  @Override
  public boolean wasHitAt(Coordinate where) {
    // TODO Auto-generated method stub
    checkCoordinateInThisShip(where);
    return myPieces.get(where);
  }

  @Override
  public T getDisplayInfoAt(Coordinate where, boolean myShip) {
    //TODO this is not right.  We need to
    //look up the hit status of this coordinate
    if (myShip) {
      return myDisplayInfo.getInfo(where, myPieces.get(where));
    } else {
      return enemyDisplayInfo.getInfo(where, myPieces.get(where));
    }
  }

  protected void checkCoordinateInThisShip(Coordinate c) {
    for (Coordinate key : myPieces.keySet()) {
      if (key.equals(c)) {
        return;
      }
    }
    throw new IllegalArgumentException("The coordinate " + c.toString() + " is not in this ship.");
  }

  @Override
  public Iterable<Coordinate> getCoordinates() {
    return myPieces.keySet();
  }

  @Override
  public void cwRotateShip() {
    Coordinate topLeft = getTopLeft();
    Coordinate temp = null;
    HashMap<Coordinate, Boolean> newMap = new HashMap<>();
    for (Coordinate c : myPieces.keySet()) {
      temp = new Coordinate(topLeft.getRow()+(c.getColumn()-topLeft.getColumn()), topLeft.getColumn()+(c.getRow() - topLeft.getRow())*-1);
      newMap.put(temp, myPieces.get(c));
//      myPieces.remove(c);
    }
    myPieces = new HashMap<>(newMap);
  }

  @Override
  public void ccwRotateShip() {
    Coordinate topLeft = getTopLeft();
    Coordinate temp = null;
    HashMap<Coordinate, Boolean> newMap = new HashMap<>();
    for (Coordinate c : myPieces.keySet()) {
      temp = new Coordinate(topLeft.getRow()+(c.getColumn()-topLeft.getColumn())*-1, topLeft.getColumn()+(c.getRow() - topLeft.getRow()));
      newMap.put(temp, myPieces.get(c));
//      myPieces.remove(c);
    }
    myPieces = new HashMap<>(newMap);
  }

}
