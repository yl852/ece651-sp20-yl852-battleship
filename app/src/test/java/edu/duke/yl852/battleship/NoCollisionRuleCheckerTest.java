package edu.duke.yl852.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class NoCollisionRuleCheckerTest {
  @Test
  public void test_checkMyRule() {
    Coordinate c = new Coordinate(1,1);
    Board b = new BattleShipBoard(5,5,'X');
    AbstractShipFactory f = new V1ShipFactory();
    Placement v1_2 = new Placement(c, 'V');
    Ship<Character> bat = f.makeSubmarine(v1_2);
    PlacementRuleChecker prc = new NoCollisionRuleChecker(null);
    assertEquals(prc.checkMyRule(bat,b)==null, true);
    Ship s = new RectangleShip(c, 1, 1);
    b.tryAddShip(s);
    assertEquals(prc.checkMyRule(bat,b)==null, false);
  }

  @Test
  public void test_checkMyRuleAll() {
    Coordinate c = new Coordinate(1,1);
    Board b = new BattleShipBoard(5,5,'X');
    AbstractShipFactory f = new V1ShipFactory();
    Placement v1 = new Placement(c, 'V');
    Ship<Character> bat = f.makeSubmarine(v1);
    PlacementRuleChecker prc = new NoCollisionRuleChecker(null);
    PlacementRuleChecker pri = new InBoundsRuleChecker(null);
    assertEquals(prc.checkMyRule(bat,b)==null&&pri.checkMyRule(bat,b)==null, true);
    c = new Coordinate(5,5);
    Placement v2 = new Placement(c, 'V');
    bat = f.makeSubmarine(v2);
    assertEquals(prc.checkMyRule(bat,b)==null&&pri.checkMyRule(bat,b)==null, false);
    c = new Coordinate(1,1);
    Placement v3 = new Placement(c, 'V');
    bat = f.makeSubmarine(v3);
    Ship s = new RectangleShip(c, 1, 1);
    b.tryAddShip(s);
    assertEquals(prc.checkMyRule(bat,b)==null&&pri.checkMyRule(bat,b)==null, false);
    InBoundsRuleChecker ibc = new InBoundsRuleChecker(null);
    PlacementRuleChecker test = new InBoundsRuleChecker(ibc);
    assertEquals(test.checkPlacement(bat,b),true);
  }

}
