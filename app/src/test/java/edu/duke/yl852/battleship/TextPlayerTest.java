package edu.duke.yl852.battleship;

import static org.junit.jupiter.api.Assertions.*;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.StringReader;
import java.io.BufferedReader;

import org.junit.jupiter.api.Test;

public class TextPlayerTest {
  @Test
  void test_read_placement() throws IOException {
    // StringReader sr = new StringReader("B2V\nC8H\na4v\n");
    // ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    // PrintStream ps = new PrintStream(bytes, true);
    // Board<Character> b = new BattleShipBoard<Character>(10, 20);
    // // App app = new App(b, sr, ps);
    // TextPlayer player = new TextPlayer("A", b, new BufferedReader(sr), ps, new V1ShipFactory());
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(10, 20, "B2V\nC8H\na4v\n", bytes);
    String prompt = "Please enter a location for a ship:";
    Placement[] expected = new Placement[3];
    expected[0] = new Placement(new Coordinate(1, 2), 'V');
    expected[1] = new Placement(new Coordinate(2, 8), 'H');
    expected[2] = new Placement(new Coordinate(0, 4), 'V');

    for (int i = 0; i < expected.length; i++) {
      Placement p = player.readPlacement(prompt);
      assertEquals(p, expected[i]); //did we get the right Placement back
      assertEquals(prompt + "\n", bytes.toString()); //should have printed prompt and newline
      bytes.reset(); //clear out bytes for next time around
    }
  }

  @Test
  void test_doOnePlacement() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(10, 20, "B2V\n", bytes);
    player.doOnePlacement();
    Placement[] expected = new Placement[1];
    expected[0] = new Placement(new Coordinate(1, 2), 'V');
    Coordinate coordinate = new Coordinate(1,2);
    assertEquals(player.theBoard.whatIsAtForSelf(coordinate), 'd'); //did we get the right Placement back
  }

  private TextPlayer createTextPlayer(int w, int h, String inputData, ByteArrayOutputStream bytes) {
    BufferedReader input = new BufferedReader(new StringReader(inputData));
    PrintStream output = new PrintStream(bytes, true);
    Board<Character> board = new BattleShipBoard<Character>(w, h,'X');
    V1ShipFactory shipFactory = new V1ShipFactory();
    return new TextPlayer("A", board, input, output, shipFactory);
  }

  @Test
  void test_doPlacementPhase() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(10, 20, "B2V\nB1V\n", bytes);
    player.doPlacementPhase();
    assertEquals(bytes.toString(), "  0|1|2|3|4|5|6|7|8|9\n" +
            "A  | | | | | | | | |  A\n" +
            "B  | | | | | | | | |  B\n" +
            "C  | | | | | | | | |  C\n" +
            "D  | | | | | | | | |  D\n" +
            "E  | | | | | | | | |  E\n" +
            "F  | | | | | | | | |  F\n" +
            "G  | | | | | | | | |  G\n" +
            "H  | | | | | | | | |  H\n" +
            "I  | | | | | | | | |  I\n" +
            "J  | | | | | | | | |  J\n" +
            "K  | | | | | | | | |  K\n" +
            "L  | | | | | | | | |  L\n" +
            "M  | | | | | | | | |  M\n" +
            "N  | | | | | | | | |  N\n" +
            "O  | | | | | | | | |  O\n" +
            "P  | | | | | | | | |  P\n" +
            "Q  | | | | | | | | |  Q\n" +
            "R  | | | | | | | | |  R\n" +
            "S  | | | | | | | | |  S\n" +
            "T  | | | | | | | | |  T\n" +
            "  0|1|2|3|4|5|6|7|8|9\n" +
            "\n" +
            "Player A: you are going to place the following ships (which are all\n" +
            "rectangular). For each ship, type the coordinate of the upper left\n" +
            "side of the ship, followed by either H (for horizontal) or V (for\n" +
            "vertical).  For example M4H would place a ship horizontally starting\n" +
            "at M4 and going to the right.\n" +
            "Player A where do you want to place a Submarine?\n" +
            "  0|1|2|3|4|5|6|7|8|9\n" +
            "A  | | | | | | | | |  A\n" +
            "B  | |s| | | | | | |  B\n" +
            "C  | |s| | | | | | |  C\n" +
            "D  | | | | | | | | |  D\n" +
            "E  | | | | | | | | |  E\n" +
            "F  | | | | | | | | |  F\n" +
            "G  | | | | | | | | |  G\n" +
            "H  | | | | | | | | |  H\n" +
            "I  | | | | | | | | |  I\n" +
            "J  | | | | | | | | |  J\n" +
            "K  | | | | | | | | |  K\n" +
            "L  | | | | | | | | |  L\n" +
            "M  | | | | | | | | |  M\n" +
            "N  | | | | | | | | |  N\n" +
            "O  | | | | | | | | |  O\n" +
            "P  | | | | | | | | |  P\n" +
            "Q  | | | | | | | | |  Q\n" +
            "R  | | | | | | | | |  R\n" +
            "S  | | | | | | | | |  S\n" +
            "T  | | | | | | | | |  T\n" +
            "  0|1|2|3|4|5|6|7|8|9\n" +
            "Player A where do you want to place a Destroyer?\n" +
            "  0|1|2|3|4|5|6|7|8|9\n" +
            "A  | | | | | | | | |  A\n" +
            "B  |d|s| | | | | | |  B\n" +
            "C  |d|s| | | | | | |  C\n" +
            "D  |d| | | | | | | |  D\n" +
            "E  | | | | | | | | |  E\n" +
            "F  | | | | | | | | |  F\n" +
            "G  | | | | | | | | |  G\n" +
            "H  | | | | | | | | |  H\n" +
            "I  | | | | | | | | |  I\n" +
            "J  | | | | | | | | |  J\n" +
            "K  | | | | | | | | |  K\n" +
            "L  | | | | | | | | |  L\n" +
            "M  | | | | | | | | |  M\n" +
            "N  | | | | | | | | |  N\n" +
            "O  | | | | | | | | |  O\n" +
            "P  | | | | | | | | |  P\n" +
            "Q  | | | | | | | | |  Q\n" +
            "R  | | | | | | | | |  R\n" +
            "S  | | | | | | | | |  S\n" +
            "T  | | | | | | | | |  T\n" +
            "  0|1|2|3|4|5|6|7|8|9\n");
  }

  @Test
  public void test_playOneTurn() throws IOException {
    Board b1 = new BattleShipBoard(5,5,'X');
    BoardTextView btv1 = new BoardTextView(b1);
    Coordinate c = new Coordinate(1,1);
    RectangleShip<Character> s1 = new RectangleShip<Character>(c, 's', '*');
    b1.tryAddShip(s1);
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(5, 5, "A1\n", bytes);
    player.playOneTurn(b1);
    assertFalse(b1.allSunk());
  }

  @Test
  public void test_playOneTurn2() throws IOException {
    Board b1 = new BattleShipBoard(5,5,'X');
    BoardTextView btv1 = new BoardTextView(b1);
    Coordinate c = new Coordinate(1,1);
    RectangleShip<Character> s1 = new RectangleShip<Character>(c, 's', '*');
    b1.tryAddShip(s1);
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(5, 5, "D4\n", bytes);
    player.playOneTurn(b1);
    assertEquals(bytes.toString(),"Player A: please enter a coordinate where you want to attack.\n"+"You missed\n");
  }

  @Test
  public void test_playOneTurn3() throws IOException {
    Board b1 = new BattleShipBoard(5,5,'X');
    BoardTextView btv1 = new BoardTextView(b1);
    Coordinate c = new Coordinate(1,1);
    RectangleShip<Character> s1 = new RectangleShip<Character>(c, 's', '*');
    b1.tryAddShip(s1);
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(5, 5, "B1\n", bytes);
    player.playOneTurn(b1);
    assertEquals(bytes.toString(),"Player A: please enter a coordinate where you want to attack.\n" +
            "  0|1|2|3|4\n" +
            "A  | | | |  A\n" +
            "B  |s| | |  B\n" +
            "C  | | | |  C\n" +
            "D  | | | |  D\n" +
            "E  | | | |  E\n" +
            "  0|1|2|3|4\n\n");
  }

  @Test
  public void test_doMovePhase() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(10, 20, "B2V\nB2V\nB1V\n", bytes);
    player.doOnePlacement();
    player.doMovePhase();
    assertEquals(player.moveCount, 2);
    player = createTextPlayer(10, 20, "B2H\nB2H\nB1V\n", bytes);
    player.doOnePlacement();
    player.doMovePhase();
    assertEquals(player.moveCount, 2);
    Board<Character> board = new BattleShipBoard<Character>(10, 20,'X');
    V1ShipFactory shipFactory = new V1ShipFactory();
    Ship<Character> ship1  = shipFactory.makeBattleship(new Placement("B2U"));
    ship1.recordHitAt(new Coordinate(2,2));
    board.tryAddShip(ship1);
    BufferedReader input = new BufferedReader(new StringReader("C2U\nB3R\n"));
    PrintStream output = new PrintStream(bytes, true);
    player = new TextPlayer("A", board, input, output, shipFactory);
    player.doMovePhase();
    assertEquals(player.moveCount, 2);
    board = new BattleShipBoard<Character>(10, 20,'X');
    ship1  = shipFactory.makeBattleship(new Placement("B2R"));
    ship1.recordHitAt(new Coordinate(2,2));
    board.tryAddShip(ship1);
    input = new BufferedReader(new StringReader("B2R\nB3U\n"));
    output = new PrintStream(bytes, true);
    player = new TextPlayer("A", board, input, output, shipFactory);
    player.doMovePhase();
    assertEquals(player.moveCount, 2);
    board = new BattleShipBoard<Character>(10, 20,'X');
    ship1  = shipFactory.makeBattleship(new Placement("B2D"));
    ship1.recordHitAt(new Coordinate(2,3));
    board.tryAddShip(ship1);
    input = new BufferedReader(new StringReader("B2D\nB3L\n"));
    output = new PrintStream(bytes, true);
    player = new TextPlayer("A", board, input, output, shipFactory);
    player.doMovePhase();
    assertEquals(player.moveCount, 2);
    board = new BattleShipBoard<Character>(10, 20,'X');
    ship1  = shipFactory.makeBattleship(new Placement("B2L"));
    ship1.recordHitAt(new Coordinate(2,2));
    board.tryAddShip(ship1);
    input = new BufferedReader(new StringReader("C2L\nB3D\n"));
    output = new PrintStream(bytes, true);
    player = new TextPlayer("A", board, input, output, shipFactory);
    player.doMovePhase();
    assertEquals(player.moveCount, 2);
    board = new BattleShipBoard<Character>(10, 20,'X');
    ship1  = shipFactory.makeSubmarine(new Placement("B2H"));
    ship1.recordHitAt(new Coordinate(1,2));
    board.tryAddShip(ship1);
    input = new BufferedReader(new StringReader("B2H\nB2V\n"));
    output = new PrintStream(bytes, true);
    player = new TextPlayer("A", board, input, output, shipFactory);
    player.doMovePhase();
    assertEquals(player.moveCount, 2);
  }

  @Test
  public void test_findUpTopLeft() {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    V1ShipFactory shipFactory = new V1ShipFactory();
    Board board = new BattleShipBoard<Character>(10, 20,'X');
    Ship ship1  = shipFactory.makeCarrier(new Placement("B2R"));
    board.tryAddShip(ship1);
    BufferedReader input = new BufferedReader(new StringReader("B2R\nB3U\n"));
    PrintStream output = new PrintStream(bytes, true);
    TextPlayer player = new TextPlayer("A", board, input, output, shipFactory);
    assertEquals(player.findUpTopLeft(ship1), new Coordinate(1,6));
    board = new BattleShipBoard<Character>(10, 20,'X');
    ship1  = shipFactory.makeCarrier(new Placement("B2U"));
    board.tryAddShip(ship1);
    input = new BufferedReader(new StringReader("B2R\nB3U\n"));
    output = new PrintStream(bytes, true);
    player = new TextPlayer("A", board, input, output, shipFactory);
    assertEquals(player.findUpTopLeft(ship1), new Coordinate(1,2));
    ship1  = shipFactory.makeCarrier(new Placement("B2D"));
    board.tryAddShip(ship1);
    input = new BufferedReader(new StringReader("B2R\nB3U\n"));
    output = new PrintStream(bytes, true);
    player = new TextPlayer("A", board, input, output, shipFactory);
    assertEquals(player.findUpTopLeft(ship1), new Coordinate(5,3));
    ship1  = shipFactory.makeCarrier(new Placement("B2L"));
    board.tryAddShip(ship1);
    input = new BufferedReader(new StringReader("B2R\nB3U\n"));
    output = new PrintStream(bytes, true);
    player = new TextPlayer("A", board, input, output, shipFactory);
    assertEquals(player.findUpTopLeft(ship1), new Coordinate(2,2));
    ship1  = shipFactory.makeBattleship(new Placement("B2L"));
    board.tryAddShip(ship1);
    input = new BufferedReader(new StringReader("B2R\nB3U\n"));
    output = new PrintStream(bytes, true);
    player = new TextPlayer("A", board, input, output, shipFactory);
    assertEquals(player.findUpTopLeft(ship1), new Coordinate(3,2));

  }

  @Test
  public void test_void() throws IOException{
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(10, 20, "B2H\nB2V\n", bytes);
    player.moveCount = 0;
    player.scanCount = 0;
    player.doMovePhase();
    player.doSonarScan();
  }

  @Test
  public void test_sonarScan() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    V1ShipFactory shipFactory = new V1ShipFactory();
    Board board = new BattleShipBoard<Character>(10, 20,'X');
    Ship ship1  = shipFactory.makeSubmarine(new Placement("B2H"));
    board.tryAddShip(ship1);
    BufferedReader input = new BufferedReader(new StringReader("B2R\nB3U\n"));
    PrintStream output = new PrintStream(bytes, true);
    TextPlayer player = new TextPlayer("A", board, input, output, shipFactory);
    player.doSonarScan();
    assertEquals(bytes.toString(), "Player A: please enter a coordinate where you want to do sonar scan.\n"+"---------------------------------------------------------------------------\n" +
            "Submarines occupy 2 squares.\n" +
            "Destroyers occupy 0 squares.\n" +
            "Battleships occupy 0 squares.\n" +
            "Carriers occupy 0 squares.\n" +
            "---------------------------------------------------------------------------\n");
  }

  @Test
  public void test_doAttackingPhase() throws IOException {
    Board b1 = new BattleShipBoard(5,5,'X');
    BoardTextView btv1 = new BoardTextView(b1);
    Coordinate c = new Coordinate(1,1);
    RectangleShip<Character> s1 = new RectangleShip<Character>(c, 's', '*');
    b1.tryAddShip(s1);
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(5, 5, "D4\n", bytes);
    player.doAttackingPhase(b1);
    assertEquals(bytes.toString(), "  0|1|2|3|4\n" +
            "A  | | | |  A\n" +
            "B  | | | |  B\n" +
            "C  | | | |  C\n" +
            "D  | | | |  D\n" +
            "E  | | | |  E\n" +
            "  0|1|2|3|4\n\n"+
            "Player A: please enter a coordinate where you want to attack.\n"+"You missed\n");
  }

  @Test
  public void test_playOneRound() throws IOException {
    Board b1 = new BattleShipBoard(5,5,'X');
    BoardTextView btv1 = new BoardTextView(b1);
    Coordinate c = new Coordinate(1,1);
    RectangleShip<Character> s1 = new RectangleShip<Character>(c, 's', '*');
    b1.tryAddShip(s1);
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(10, 20, "S\nA3\nF\nD4\nA1V\nM\nA1\nA2V\n", bytes);
    player.playOneRound(b1);
    assertEquals(bytes.toString(), "---------------------------------------------------------------------------\n" +
            "Possible actions for Player A:\n\n"+
            " F Fire at a square\n" +
            " M Move a ship to another square ("+3+" remaining)\n" +
            " S Sonar scan ("+3+" remaining)\n\n"+
            "Player A, what would you like to do?\n" +
            "---------------------------------------------------------------------------\n"+
            "Player " + "A" + ": please enter a coordinate where you want to do sonar scan.\n"+
            "---------------------------------------------------------------------------\n"+
            "Submarines occupy 0 squares.\n" +
            "Destroyers occupy 0 squares.\n" +
            "Battleships occupy 0 squares.\n" +
            "Carriers occupy 0 squares.\n" +
            "---------------------------------------------------------------------------\n");
    player.playOneRound(b1);
    player.doOnePlacement();
    player.playOneRound(b1);
  }
}
