package edu.duke.yl852.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class V1ShipFactoryTest {

  private void checkShip(Ship<Character> testShip, String expectedName, char expectedLetter, Coordinate... expectedLocs) {
    assertEquals(testShip.getName(), expectedName);
    for (Coordinate c : expectedLocs) {
      assertTrue(testShip.occupiesCoordinates(c));
    }
  }
  
  //writing test case for all kinds of ships and orientations
  @Test
  public void test_createShip() {
    AbstractShipFactory f = new V1ShipFactory();
    Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
    Placement v1_3 = new Placement(new Coordinate(1, 1), 'U');
    Placement v1_4 = new Placement(new Coordinate(1, 1), 'R');
    Placement v1_5 = new Placement(new Coordinate(1, 1), 'D');
    Placement v1_6 = new Placement(new Coordinate(1, 1), 'L');
    Ship<Character> dst = f.makeDestroyer(v1_2);
    checkShip(dst, "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2));
    Ship<Character> bat = f.makeBattleship(v1_3);
    checkShip(bat, "BattleShip", 'b', new Coordinate(1, 2), new Coordinate(2, 1), new Coordinate(2, 2), new Coordinate(2,3));
    Ship<Character> car = f.makeCarrier(v1_3);
    checkShip(car, "Carrier", 'c', new Coordinate(1, 1), new Coordinate(2, 1), new Coordinate(3, 1),
            new Coordinate(4, 1), new Coordinate(3, 2), new Coordinate(4, 2), new Coordinate(5, 2));
    Ship<Character> sub = f.makeSubmarine(v1_2);
    checkShip(sub, "Submarine", 's', new Coordinate(1, 2), new Coordinate(2, 2));

    Placement v1_2_1 = new Placement(new Coordinate(1, 2), 'H');
    Ship<Character> dst1 = f.makeDestroyer(v1_2_1);
    checkShip(dst1, "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(1, 4));

    bat = f.makeBattleship(v1_4);
    checkShip(bat, "BattleShip", 'b', new Coordinate(1, 1), new Coordinate(2, 1), new Coordinate(3, 1), new Coordinate(2,2));
    car = f.makeCarrier(v1_4);
    checkShip(car, "Carrier", 'c', new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(1, 4),
            new Coordinate(1, 5), new Coordinate(2, 1), new Coordinate(2, 2), new Coordinate(2, 3));

    bat = f.makeBattleship(v1_5);
    checkShip(bat, "BattleShip", 'b', new Coordinate(1, 1), new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(2,2));
    car = f.makeCarrier(v1_5);
    checkShip(car, "Carrier", 'c', new Coordinate(1, 1), new Coordinate(2, 1), new Coordinate(3, 1),
            new Coordinate(2, 2), new Coordinate(3, 2), new Coordinate(4, 2), new Coordinate(5, 2));

    bat = f.makeBattleship(v1_6);
    checkShip(bat, "BattleShip", 'b', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2), new Coordinate(2,1));
    car = f.makeCarrier(v1_6);
    checkShip(car, "Carrier", 'c', new Coordinate(1, 3), new Coordinate(1, 4), new Coordinate(1, 5),
            new Coordinate(2, 1), new Coordinate(2, 2), new Coordinate(2, 3), new Coordinate(2, 4));

  }

}
