package edu.duke.yl852.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class InBoundsRuleCheckerTest {
  @Test
  public void test_checkMyRule() {
    Coordinate c = new Coordinate(100,100);
    Ship s = new RectangleShip(c, 1, 2);
    Board b = new BattleShipBoard(3,3,'X');
    PlacementRuleChecker prc = new InBoundsRuleChecker(null);
    assertEquals(prc.checkMyRule(s, b)==null, false);
    Coordinate c1 = new Coordinate(1,1);
    Ship s1 = new RectangleShip(c1, 1, 2);
    assertEquals(prc.checkMyRule(s1, b)==null, true);
    Coordinate c2 = new Coordinate(-1,2);
    Ship s2 = new RectangleShip(c2, 1, 2);
    assertEquals(prc.checkMyRule(s2, b)==null, false);
    Coordinate c3 = new Coordinate(1,-2);
    Ship s3 = new RectangleShip(c3, 1, 2);
    assertEquals(prc.checkMyRule(s3, b)==null, false);
    Coordinate c4 = new Coordinate(1,200);
    Ship s4 = new RectangleShip(c4, 1, 2);
    assertEquals(prc.checkMyRule(s4, b)==null, false);
  }

  @Test
  public void test_checkPlacement() {
    Coordinate c = new Coordinate(100,100);
    Ship s = new RectangleShip(c, 1, 2);
    Board b = new BattleShipBoard(5,5,'X');
    PlacementRuleChecker prc = new InBoundsRuleChecker(null);
    assertEquals(prc.checkPlacement(s, b), false);

    Coordinate c1 = new Coordinate(1,1);
    AbstractShipFactory f = new V1ShipFactory();
    Placement v1_2 = new Placement(c1, 'V');
    Ship<Character> bat = f.makeSubmarine(v1_2);
    assertEquals(prc.checkPlacement(bat, b), true);

    Ship<Character> bat1 = null;
    assertEquals(prc.checkPlacement(bat, b), true);
  }

}
