package edu.duke.yl852.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class SimpleShipDisplayInfoTest {
  @Test
  public void test_getInfo() {
    ShipDisplayInfo s1 = new SimpleShipDisplayInfo(1, 2);
    Coordinate c1 = new Coordinate(1,2);
    assertEquals(s1.getInfo(c1, true), 2);
  }

}
