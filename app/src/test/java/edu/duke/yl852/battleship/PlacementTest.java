package edu.duke.yl852.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class PlacementTest {
  @Test
  public void test_Placement_Equals() {
    Coordinate c1 = new Coordinate("B3");
    Placement p1 = new Placement(c1, 'v');
    Placement p2 = new Placement(c1, 'V');
    Placement p3 = new Placement("b3v");
    Placement p4 = new Placement("B3V");
    assertEquals(c1, p1.getCoordiante());
    assertEquals('V', p1.getOrientation());
    assertEquals(p1, p2);
    assertEquals(p3, p4);
    assertEquals(p1, p3);
    assertNotEquals(p1, "(1, 3, V)");
  }

  @Test
  public void test_hashCode() {
    Coordinate c1 = new Coordinate("B3");
    Placement p1 = new Placement(c1, 'V');
    Placement p2 = new Placement("b3v");
    Placement p3 = new Placement(c1, 'H');
    Placement p4 = new Placement(c1, 'v');
    assertEquals(p1.hashCode(), p2.hashCode());
    assertEquals(p1.hashCode(), p4.hashCode());
    assertNotEquals(p1.hashCode(), p3.hashCode());
  }

  @Test
  public void test_string_constructor_error_cases() {
    assertThrows(IllegalArgumentException.class, () -> new Placement("00"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("A00"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("@0V"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("S0/"));
  }


}
