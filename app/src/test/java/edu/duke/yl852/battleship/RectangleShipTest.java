package edu.duke.yl852.battleship;

import static org.junit.jupiter.api.Assertions.*;

import java.util.HashSet;
import java.util.HashMap;

import org.junit.jupiter.api.Test;

public class RectangleShipTest {
  @Test
  public void test_makeCoords() {
    RectangleShip<Character> r1 = new RectangleShip<Character>(new Coordinate(1,2), 's', '*');
    HashSet<Coordinate> hashSet = r1.makeCoords(new Coordinate(1,2), 1, 3);
    HashSet<Coordinate> expected = new HashSet<>();
    expected.add(new Coordinate(1,2));
    expected.add(new Coordinate(2,2));
    expected.add(new Coordinate(3,2));
    assertEquals(hashSet, expected);
  }

  @Test
  public void test_rectangleShip() {
    Basicship r1 = new RectangleShip(new Coordinate(1,2), 1, 3);
    HashMap<Coordinate, Boolean> expected = new HashMap<>();
    expected.put(new Coordinate(1,2), false);
    assertEquals(r1.myPieces, expected);
//    r1 = new RectangleShip(new Coordinate(1,2), )
  }

  @Test
  public void test_hitAt() {
    Basicship s1 = new RectangleShip(new Coordinate(1,2), 1, 3);
    s1.recordHitAt(new Coordinate(1,2));
    HashMap<Coordinate, Boolean> expected = new HashMap<>();
    expected.put(new Coordinate(1,2), true);
    assertEquals(s1.wasHitAt(new Coordinate(1,2)), true);
    assertEquals(s1.myPieces, expected);
  }

  @Test
  public void test_isSunk() {
    Basicship s1 = new RectangleShip(new Coordinate(1,2), 1, 3);
    assertEquals(s1.isSunk(), false);
    s1.recordHitAt(new Coordinate(1,2));
    assertEquals(s1.isSunk(), true);
  }

  @Test
  public void test_getDisplayInfoAt() {
    Basicship s1 = new RectangleShip(new Coordinate(1,2), 1, 3);
    assertEquals(s1.getDisplayInfoAt(new Coordinate(1,2), true), 1);
    s1.recordHitAt(new Coordinate(1,2));
    assertEquals(s1.getDisplayInfoAt(new Coordinate(1,2), true), 3);
    assertNotEquals(s1.getDisplayInfoAt(new Coordinate(1,2), false),3);
  }

  @Test
  public void test_checkCoordinateInThisShip() {
    Basicship s1 = new RectangleShip(new Coordinate(1,2), 1, 3);
    assertThrows(IllegalArgumentException.class, () -> s1.checkCoordinateInThisShip(new Coordinate(2,4)));
  }

  @Test
  public void test_getName() {
    Basicship s1 = new RectangleShip(new Coordinate(1,2), 1, 3);
    assertEquals(s1.getName(), "testship");
  }

}
