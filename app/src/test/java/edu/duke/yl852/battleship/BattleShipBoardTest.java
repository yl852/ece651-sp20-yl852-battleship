package edu.duke.yl852.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class BattleShipBoardTest {
  @Test
  public void test_width_and_height() {
    Board<Character> b1 = new BattleShipBoard(10, 20, 'X');
    assertEquals(10, b1.getWidth());
    assertEquals(20, b1.getHeight());
  }

  @Test
  public void test_invalid_dimensions() {
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard(10, 0, 'X'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard(0, 20,'X'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard(10, -5,'X'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard(-8, 20,'X'));
  }

  private <T> void checkWhatIsAtBoard(BattleShipBoard<T> b, T[][] expect) {
      Coordinate location = new Coordinate(0,0);
      for (int i = 0; i < b.getHeight(); i++) {
        for(int j = 0; j < b.getWidth(); j++) {
          location = new Coordinate(i,j);
          assertEquals(b.whatIsAtForSelf(location), expect[i][j]);
        }
      }
  }

  @Test
  public void test_whatIsAt() {
    BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(2,3,'X');
    Character[][] expect = new Character[3][4];
    for (int i = 0; i < 2; i++) {
      for (int j = 0; j < 3; j++) {
        expect[i][j] = null;
      }
    }
    checkWhatIsAtBoard(b1, expect);

    BattleShipBoard<Character> b2 = new BattleShipBoard<Character>(2,3,'X');
    Coordinate where = new Coordinate(0,0);
    //Ship<Character> s1 = new Basicship(where);
    RectangleShip<Character> s1 = new RectangleShip<Character>(where, 's', '*');
    assertTrue(b2.tryAddShip(s1)==null);
    expect[0][0] = 's';
    checkWhatIsAtBoard(b2, expect);
  }

  @Test
  public void test_tryAddShip() {
    Coordinate c = new Coordinate(1,1);
    Board b = new BattleShipBoard(5,5,'X');
    AbstractShipFactory f = new V1ShipFactory();
    Placement v1 = new Placement(c, 'V');
    Ship<Character> sub = f.makeSubmarine(v1);
    assertEquals(b.tryAddShip(sub), null);
    Ship s = f.makeSubmarine(v1);
    //b.tryAddShip(s);
    assertEquals(b.tryAddShip(s), "Already a ship at this location");
    c = new Coordinate(10,10);
    v1 = new Placement(c, 'V');
    sub = f.makeSubmarine(v1);
    assertEquals(b.tryAddShip(sub), "That placement is invalid: the ship goes off the bottom of the board.");

  }

  @Test
  public void test_fireAt() {
    Board b = new BattleShipBoard(5,5,'X');
    Coordinate c = new Coordinate(1,1);
    AbstractShipFactory f = new V1ShipFactory();
    Placement v1 = new Placement(c, 'V');
    Ship<Character> bat = f.makeSubmarine(v1);
    b.tryAddShip(bat);
    assertEquals(b.fireAt(new Coordinate(5,5)), null);
    assertEquals(b.fireAt(new Coordinate(1,1)), bat);
  }

  @Test
  public void test_whatIsAtForEnemy() {
    Board b = new BattleShipBoard(5,5,'X');
    Coordinate c = new Coordinate(1,1);
    AbstractShipFactory f = new V1ShipFactory();
    Placement v1 = new Placement(c, 'V');
    Ship<Character> bat = f.makeSubmarine(v1);
    b.tryAddShip(bat);
    assertEquals(b.whatIsAtForEnemy(new Coordinate(1,1)), null);
    b.fireAt(new Coordinate(3,3));
    assertEquals(b.whatIsAtForEnemy(new Coordinate(3,3)), 'X');
  }

  @Test
  public void test_allSunk() {
    Board b = new BattleShipBoard(5,5,'X');
    Coordinate c = new Coordinate(1,1);
    RectangleShip<Character> s1 = new RectangleShip<Character>(c, 's', '*');
    b.tryAddShip(s1);
    b.fireAt(c);
    assertTrue(b.allSunk());
  }

  @Test
  public void test_removeShip() {
    Board b = new BattleShipBoard(5,5,'X');
    Coordinate c = new Coordinate(1,1);
    RectangleShip<Character> s1 = new RectangleShip<Character>(c, 's', '*');
    b.tryAddShip(s1);
    assertNull(b.removeShip(new Coordinate(3,3)));
  }


}
