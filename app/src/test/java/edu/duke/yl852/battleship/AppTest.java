package edu.duke.yl852.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.StringReader;
import java.io.BufferedReader;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.parallel.*;
public class AppTest {

//  @Disabled
  @Test
  @ResourceLock(value = Resources.SYSTEM_OUT, mode = ResourceAccessMode.READ_WRITE)
  void test_main() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    PrintStream out = new PrintStream(bytes, true);
    InputStream input = getClass().getClassLoader().getResourceAsStream("input.txt");
    assertNotNull(input);
    InputStream expectedStream = getClass().getClassLoader().getResourceAsStream("output.txt");
    assertNotNull(expectedStream);
    InputStream oldIn = System.in;
    PrintStream oldOut = System.out;
    try {
      System.setIn(input);
      System.setOut(out);
      App.main(new String[0]);
    }
    finally {
      System.setIn(oldIn);
      System.setOut(oldOut);
    }
    String expected = new String(expectedStream.readAllBytes());
    String actual = bytes.toString();
    assertEquals(expected, actual);
  }

  @Test
  @ResourceLock(value = Resources.SYSTEM_OUT, mode = ResourceAccessMode.READ_WRITE)
  void test_main2() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    PrintStream out = new PrintStream(bytes, true);
    InputStream input = getClass().getClassLoader().getResourceAsStream("input1.txt");
    assertNotNull(input);
    InputStream expectedStream = getClass().getClassLoader().getResourceAsStream("output1.txt");
    assertNotNull(expectedStream);
    InputStream oldIn = System.in;
    PrintStream oldOut = System.out;
    try {
      System.setIn(input);
      System.setOut(out);
      App.main(new String[0]);
    }
    finally {
      System.setIn(oldIn);
      System.setOut(oldOut);
    }
    String expected = new String(expectedStream.readAllBytes());
    String actual = bytes.toString();
    assertEquals(expected, actual);
  }


  private TextPlayer createTextPlayer(int w, int h, String inputData, ByteArrayOutputStream bytes) {
    BufferedReader input = new BufferedReader(new StringReader(inputData));
    PrintStream output = new PrintStream(bytes, true);
    Board<Character> board = new BattleShipBoard<Character>(w, h,'X');
    V1ShipFactory shipFactory = new V1ShipFactory();
    return new TextPlayer("A", board, input, output, shipFactory);
  }
  @Test
  public void test_result_check() throws IOException{
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player1 = createTextPlayer(10, 20, "B2V\n", bytes);
    player1.doOnePlacement();
    TextPlayer player2 = createTextPlayer(10, 20, "B1V\nB2\nC2\nD2", bytes);
    player2.doOnePlacement();
    player2.doAttackingPhase(player1.theBoard);
    player2.doAttackingPhase(player1.theBoard);
    player2.doAttackingPhase(player1.theBoard);
    App app = new App(player1, player2);
    app.result_check();
    assertEquals(bytes.toString(), "Player A where do you want to place a Destroyer?\n" +
            "  0|1|2|3|4|5|6|7|8|9\n" +
            "A  | | | | | | | | |  A\n" +
            "B  | |d| | | | | | |  B\n" +
            "C  | |d| | | | | | |  C\n" +
            "D  | |d| | | | | | |  D\n" +
            "E  | | | | | | | | |  E\n" +
            "F  | | | | | | | | |  F\n" +
            "G  | | | | | | | | |  G\n" +
            "H  | | | | | | | | |  H\n" +
            "I  | | | | | | | | |  I\n" +
            "J  | | | | | | | | |  J\n" +
            "K  | | | | | | | | |  K\n" +
            "L  | | | | | | | | |  L\n" +
            "M  | | | | | | | | |  M\n" +
            "N  | | | | | | | | |  N\n" +
            "O  | | | | | | | | |  O\n" +
            "P  | | | | | | | | |  P\n" +
            "Q  | | | | | | | | |  Q\n" +
            "R  | | | | | | | | |  R\n" +
            "S  | | | | | | | | |  S\n" +
            "T  | | | | | | | | |  T\n" +
            "  0|1|2|3|4|5|6|7|8|9\n" +
            "\n" +
            "Player A where do you want to place a Destroyer?\n" +
            "  0|1|2|3|4|5|6|7|8|9\n" +
            "A  | | | | | | | | |  A\n" +
            "B  |d| | | | | | | |  B\n" +
            "C  |d| | | | | | | |  C\n" +
            "D  |d| | | | | | | |  D\n" +
            "E  | | | | | | | | |  E\n" +
            "F  | | | | | | | | |  F\n" +
            "G  | | | | | | | | |  G\n" +
            "H  | | | | | | | | |  H\n" +
            "I  | | | | | | | | |  I\n" +
            "J  | | | | | | | | |  J\n" +
            "K  | | | | | | | | |  K\n" +
            "L  | | | | | | | | |  L\n" +
            "M  | | | | | | | | |  M\n" +
            "N  | | | | | | | | |  N\n" +
            "O  | | | | | | | | |  O\n" +
            "P  | | | | | | | | |  P\n" +
            "Q  | | | | | | | | |  Q\n" +
            "R  | | | | | | | | |  R\n" +
            "S  | | | | | | | | |  S\n" +
            "T  | | | | | | | | |  T\n" +
            "  0|1|2|3|4|5|6|7|8|9\n" +
            "\n" +
            "  0|1|2|3|4|5|6|7|8|9\n" +
            "A  | | | | | | | | |  A\n" +
            "B  | | | | | | | | |  B\n" +
            "C  | | | | | | | | |  C\n" +
            "D  | | | | | | | | |  D\n" +
            "E  | | | | | | | | |  E\n" +
            "F  | | | | | | | | |  F\n" +
            "G  | | | | | | | | |  G\n" +
            "H  | | | | | | | | |  H\n" +
            "I  | | | | | | | | |  I\n" +
            "J  | | | | | | | | |  J\n" +
            "K  | | | | | | | | |  K\n" +
            "L  | | | | | | | | |  L\n" +
            "M  | | | | | | | | |  M\n" +
            "N  | | | | | | | | |  N\n" +
            "O  | | | | | | | | |  O\n" +
            "P  | | | | | | | | |  P\n" +
            "Q  | | | | | | | | |  Q\n" +
            "R  | | | | | | | | |  R\n" +
            "S  | | | | | | | | |  S\n" +
            "T  | | | | | | | | |  T\n" +
            "  0|1|2|3|4|5|6|7|8|9\n" +
            "\n" +
            "Player A: please enter a coordinate where you want to attack.\n" +
            "  0|1|2|3|4|5|6|7|8|9\n" +
            "A  | | | | | | | | |  A\n" +
            "B  | |d| | | | | | |  B\n" +
            "C  | | | | | | | | |  C\n" +
            "D  | | | | | | | | |  D\n" +
            "E  | | | | | | | | |  E\n" +
            "F  | | | | | | | | |  F\n" +
            "G  | | | | | | | | |  G\n" +
            "H  | | | | | | | | |  H\n" +
            "I  | | | | | | | | |  I\n" +
            "J  | | | | | | | | |  J\n" +
            "K  | | | | | | | | |  K\n" +
            "L  | | | | | | | | |  L\n" +
            "M  | | | | | | | | |  M\n" +
            "N  | | | | | | | | |  N\n" +
            "O  | | | | | | | | |  O\n" +
            "P  | | | | | | | | |  P\n" +
            "Q  | | | | | | | | |  Q\n" +
            "R  | | | | | | | | |  R\n" +
            "S  | | | | | | | | |  S\n" +
            "T  | | | | | | | | |  T\n" +
            "  0|1|2|3|4|5|6|7|8|9\n" +
            "\n" +
            "  0|1|2|3|4|5|6|7|8|9\n" +
            "A  | | | | | | | | |  A\n" +
            "B  | |d| | | | | | |  B\n" +
            "C  | | | | | | | | |  C\n" +
            "D  | | | | | | | | |  D\n" +
            "E  | | | | | | | | |  E\n" +
            "F  | | | | | | | | |  F\n" +
            "G  | | | | | | | | |  G\n" +
            "H  | | | | | | | | |  H\n" +
            "I  | | | | | | | | |  I\n" +
            "J  | | | | | | | | |  J\n" +
            "K  | | | | | | | | |  K\n" +
            "L  | | | | | | | | |  L\n" +
            "M  | | | | | | | | |  M\n" +
            "N  | | | | | | | | |  N\n" +
            "O  | | | | | | | | |  O\n" +
            "P  | | | | | | | | |  P\n" +
            "Q  | | | | | | | | |  Q\n" +
            "R  | | | | | | | | |  R\n" +
            "S  | | | | | | | | |  S\n" +
            "T  | | | | | | | | |  T\n" +
            "  0|1|2|3|4|5|6|7|8|9\n" +
            "\n" +
            "Player A: please enter a coordinate where you want to attack.\n" +
            "  0|1|2|3|4|5|6|7|8|9\n" +
            "A  | | | | | | | | |  A\n" +
            "B  | |d| | | | | | |  B\n" +
            "C  | |d| | | | | | |  C\n" +
            "D  | | | | | | | | |  D\n" +
            "E  | | | | | | | | |  E\n" +
            "F  | | | | | | | | |  F\n" +
            "G  | | | | | | | | |  G\n" +
            "H  | | | | | | | | |  H\n" +
            "I  | | | | | | | | |  I\n" +
            "J  | | | | | | | | |  J\n" +
            "K  | | | | | | | | |  K\n" +
            "L  | | | | | | | | |  L\n" +
            "M  | | | | | | | | |  M\n" +
            "N  | | | | | | | | |  N\n" +
            "O  | | | | | | | | |  O\n" +
            "P  | | | | | | | | |  P\n" +
            "Q  | | | | | | | | |  Q\n" +
            "R  | | | | | | | | |  R\n" +
            "S  | | | | | | | | |  S\n" +
            "T  | | | | | | | | |  T\n" +
            "  0|1|2|3|4|5|6|7|8|9\n" +
            "\n" +
            "  0|1|2|3|4|5|6|7|8|9\n" +
            "A  | | | | | | | | |  A\n" +
            "B  | |d| | | | | | |  B\n" +
            "C  | |d| | | | | | |  C\n" +
            "D  | | | | | | | | |  D\n" +
            "E  | | | | | | | | |  E\n" +
            "F  | | | | | | | | |  F\n" +
            "G  | | | | | | | | |  G\n" +
            "H  | | | | | | | | |  H\n" +
            "I  | | | | | | | | |  I\n" +
            "J  | | | | | | | | |  J\n" +
            "K  | | | | | | | | |  K\n" +
            "L  | | | | | | | | |  L\n" +
            "M  | | | | | | | | |  M\n" +
            "N  | | | | | | | | |  N\n" +
            "O  | | | | | | | | |  O\n" +
            "P  | | | | | | | | |  P\n" +
            "Q  | | | | | | | | |  Q\n" +
            "R  | | | | | | | | |  R\n" +
            "S  | | | | | | | | |  S\n" +
            "T  | | | | | | | | |  T\n" +
            "  0|1|2|3|4|5|6|7|8|9\n" +
            "\n" +
            "Player A: please enter a coordinate where you want to attack.\n" +
            "  0|1|2|3|4|5|6|7|8|9\n" +
            "A  | | | | | | | | |  A\n" +
            "B  | |d| | | | | | |  B\n" +
            "C  | |d| | | | | | |  C\n" +
            "D  | |d| | | | | | |  D\n" +
            "E  | | | | | | | | |  E\n" +
            "F  | | | | | | | | |  F\n" +
            "G  | | | | | | | | |  G\n" +
            "H  | | | | | | | | |  H\n" +
            "I  | | | | | | | | |  I\n" +
            "J  | | | | | | | | |  J\n" +
            "K  | | | | | | | | |  K\n" +
            "L  | | | | | | | | |  L\n" +
            "M  | | | | | | | | |  M\n" +
            "N  | | | | | | | | |  N\n" +
            "O  | | | | | | | | |  O\n" +
            "P  | | | | | | | | |  P\n" +
            "Q  | | | | | | | | |  Q\n" +
            "R  | | | | | | | | |  R\n" +
            "S  | | | | | | | | |  S\n" +
            "T  | | | | | | | | |  T\n" +
            "  0|1|2|3|4|5|6|7|8|9\n" +
            "\n");
  }

}
