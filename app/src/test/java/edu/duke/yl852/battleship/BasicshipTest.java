package edu.duke.yl852.battleship;

import static org.junit.jupiter.api.Assertions.*;
import java.util.HashSet;
import org.junit.jupiter.api.Test;

public class BasicshipTest {
  @Test
  public void test_getCoordinates() {
    HashSet<Coordinate> coord = new HashSet<>();
    coord.add(new Coordinate(1,1));
    Ship s = new RectangleShip(new Coordinate(1,1), 1,1);
    assertEquals(s.getCoordinates(), coord);
  }

}
