package edu.duke.yl852.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class BoardTextViewTest {
  @Test
  public void test_display_empty_2by2() {
    Board<Character> b1 = new BattleShipBoard(2, 2,'X');
    BoardTextView view = new BoardTextView(b1);
    String expectedHeader= "  0|1\n";
    assertEquals(expectedHeader, view.makeHeader());
    String expected=
      expectedHeader+
      "A  |  A\n"+
      "B  |  B\n"+
      expectedHeader;
    assertEquals(expected, view.displayMyOwnBoard());
  }

  @Test
  public void test_invalid_board_size() {
    Board<Character> wideBoard = new BattleShipBoard(11,20,'X');
    Board<Character> tallBoard = new BattleShipBoard(10,27,'X');
    assertThrows(IllegalArgumentException.class, () -> new BoardTextView(wideBoard));
    assertThrows(IllegalArgumentException.class, () -> new BoardTextView(tallBoard));
  }

  @Test
  public void test_display_empty_3by2() {
    Board<Character> b2 = new BattleShipBoard(2, 3,'X');
    BoardTextView view = new BoardTextView(b2);
    String expectedHeader= "  0|1\n";
    String expected=
      expectedHeader+
      "A  |  A\n"+
      "B  |  B\n"+
      "C  |  C\n"+
      expectedHeader;
    assertEquals(expected, view.displayMyOwnBoard());
  }

  @Test
  public void test_display_empty_3by5() {
    Board<Character> b3 = new BattleShipBoard(5, 3,'X');
    BoardTextView view = new BoardTextView(b3);
    String expectedHeader= "  0|1|2|3|4\n";
    String expected=
      expectedHeader+
      "A  | | | |  A\n"+
      "B  | | | |  B\n"+
      "C  | | | |  C\n"+
      expectedHeader;
    assertEquals(expected, view.displayMyOwnBoard());
  }

  private void emptyBoardHelper(int w, int h, String expectedHeader, String expectedBody) {
    Board<Character> b1 = new BattleShipBoard(w,h,'X');
    BoardTextView view = new BoardTextView(b1);
    assertEquals(expectedHeader, view.makeHeader());
    String expected = expectedHeader + expectedBody + expectedHeader;
    assertEquals(expected, view.displayMyOwnBoard());
  }

  private void filledBoardHelper(int w, int h, String expectedHeader, String expectedBody) {
    Board<Character> b1 = new BattleShipBoard(w,h,'X');
    Coordinate loc = new Coordinate(0,0);
    //Ship<Character> toAdd = new Basicship(loc);
    Ship<Character> toAdd = new RectangleShip<Character>(loc, 's', '*');
    b1.tryAddShip(toAdd);
    BoardTextView view = new BoardTextView(b1);
    assertEquals(expectedHeader, view.makeHeader());
    String expected = expectedHeader + expectedBody + expectedHeader;
    assertEquals(expected, view.displayMyOwnBoard());
  }
  

  @Test
  public void test_display_empty_2by8() {
    String body =
    "A  | | | | | | |  A\n"+
    "B  | | | | | | |  B\n";
    emptyBoardHelper(8,2,"  0|1|2|3|4|5|6|7\n", body);
  }

  @Test
  public void test_Board_With_Ship() {
    String body = 
      "A s|  A\n"+
      "B  |  B\n"+
      "C  |  C\n";
    filledBoardHelper(2,3,"  0|1\n", body);
  }

  @Test
  public void test_displayEnemyBoard() {
    Board b = new BattleShipBoard(5,5,'X');
    Coordinate c = new Coordinate(1,1);
    AbstractShipFactory f = new V1ShipFactory();
    Placement v1 = new Placement(c, 'V');
    Ship<Character> bat = f.makeSubmarine(v1);
    b.tryAddShip(bat);
    BoardTextView btv = new BoardTextView(b);
    String expectedHeader= "  0|1|2|3|4\n";
    String expected=
            expectedHeader+
                    "A  | | | |  A\n"+
                    "B  | | | |  B\n"+
                    "C  | | | |  C\n"+
                    "D  | | | |  D\n"+
                    "E  | | | |  E\n"+
                    expectedHeader;
    assertEquals(btv.displayEnemyBoard(), expected);
  }

  @Test
  public void test_displayMyOwnBoard() {
    Board b = new BattleShipBoard(5,5,'X');
    Coordinate c = new Coordinate(1,1);
    AbstractShipFactory f = new V1ShipFactory();
    Placement v1 = new Placement(c, 'V');
    Ship<Character> bat = f.makeSubmarine(v1);
    b.tryAddShip(bat);
    BoardTextView btv = new BoardTextView(b);
    String expectedHeader= "  0|1|2|3|4\n";
    String expected=
            expectedHeader+
                    "A  | | | |  A\n"+
                    "B  |s| | |  B\n"+
                    "C  |s| | |  C\n"+
                    "D  | | | |  D\n"+
                    "E  | | | |  E\n"+
                    expectedHeader;
    assertEquals(btv.displayMyOwnBoard(), expected);
  }

  @Test
  public void test_displayMyBoardWithEnemyNextToIt() {
    Board b1 = new BattleShipBoard(5,5,'X');
    Board b2 = new BattleShipBoard(5,5,'X');
    BoardTextView btv1 = new BoardTextView(b1);
    BoardTextView btv2 = new BoardTextView(b2);
    String expectedHeader= "  0|1|2|3|4                 0|1|2|3|4\n";
    String expected=
            expectedHeader+
                    "A  | | | |  A             A  | | | |  A\n"+
                    "B  | | | |  B             B  | | | |  B\n"+
                    "C  | | | |  C             C  | | | |  C\n"+
                    "D  | | | |  D             D  | | | |  D\n"+
                    "E  | | | |  E             E  | | | |  E\n"+
                    expectedHeader;
    assertEquals(btv2.displayMyBoardWithEnemyNextToIt(btv1," ", " "),expected);
  }

}
