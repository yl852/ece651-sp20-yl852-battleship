package edu.duke.yl852.battleship;

import java.util.ArrayList;
import java.util.HashSet;

/**
   * Constructs a BattleShipBoard with the specified width
   * and height
   * @param w is the width of the newly constructed board.
   * @param h is the height of the newly constructed board.
   * @throws IllegalArgumentException if the width or height are less than or equal to zero.
   */

public class BattleShipBoard<T> implements Board<T>{
    private final int width;
    private final int height;
    final ArrayList<Ship<T>> myShips;
    private final PlacementRuleChecker<T> placementChecker;
    HashSet<Coordinate> enemyMisses;

    public BattleShipBoard(int w, int h, PlacementRuleChecker<T> placementRuleChecker) {
        if (w <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's width must be positive but is " + w);
          }
          if (h <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's height must be positive but is " + h);
          }
      
        this.width = w;
        this.height = h;
        myShips = new ArrayList<>();
        this.placementChecker = placementRuleChecker;
    }

    public BattleShipBoard(int w, int h) {
        this(w, h, new InBoundsRuleChecker<T>(null));
    }
    

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    //Takes a ship (Basicship for now) object as a input and add to the myShips arrylist
    public String tryAddShip(Ship<T> toAdd) {
        //Check if the ship being added is at valid place
        PlacementRuleChecker nrc = new NoCollisionRuleChecker(null);
        PlacementRuleChecker irc = new InBoundsRuleChecker(null);
        Board board = new BattleShipBoard(this.width, this.height);
        for (Ship s : myShips) {
            board.tryAddShip(s);
        }
        if (nrc.checkMyRule(toAdd, board)!=null) {
            return nrc.checkMyRule(toAdd, board);
        } else if (irc.checkMyRule(toAdd, board)!=null) {
            return irc.checkMyRule(toAdd, board);
        }
        myShips.add(toAdd);
        return null;
    }
    
    //Takes a coordinate object as input to display if there is a ship at the target coordinate
    public T whatIsAtForSelf(Coordinate where) {
        return whatIsAt(where, true);
    }

    protected T whatIsAt(Coordinate where, boolean isSelf) {
        for (Ship<T> s:myShips) {
            if (s.occupiesCoordinates(where)) {
                return s.getDisplayInfoAt(where, isSelf);
            }
        }
        return null;
    }

    public Ship<T> fireAt(Coordinate c) {
        for (Ship<T> s : myShips) {
            if (s.occupiesCoordinates(c)) {
                s.recordHitAt(c);
                return s;
            }
        }
        enemyMisses.add(c);
        return null;
    }

}
